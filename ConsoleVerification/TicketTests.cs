﻿using HelpDesk.Controllers;
using HelpDesk.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AppTests
{

    class TicketTests
    {
        [Test]
        public void ConstructorDescriptionAndNoDueDate()
        {
            var inputDesc = "I have problems with something.";

            // Act
            var ticket = new Ticket(inputDesc, null);

            // Assert
            Assert.That(ticket.Description, Is.EqualTo(inputDesc));
            Assert.That(ticket.DueDate, Is.EqualTo(null));
        }

        [Test]
        public void ConstructorDescriptionAndDueDate()
        {
            var inputDesc = "I have problems with something.";
            var inputDate = new DateTime(2018, 2, 3);

            // Act
            var ticket = new Ticket(inputDesc, inputDate);

            // Assert
            Assert.That(ticket.Description, Is.EqualTo(inputDesc));
            Assert.That(ticket.DueDate, Is.EqualTo(inputDate));
        }

        [Test]
        public void CrerteAndMarkTicketSolved()
        {
            var inputDesc = "Hello, I have problems with...";

            var ticket = new Ticket(inputDesc, null);

            // Act
            ticket.MarkSolved();

            // Assert
            Assert.That(ticket.Solved, Is.True);
        }
    }

}

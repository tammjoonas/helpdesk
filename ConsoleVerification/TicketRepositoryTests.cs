﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Controllers;
using HelpDesk.Models;
using HelpDesk.Repositories;
using NUnit.Framework;

namespace AppTests
{
    class TicketRepositoryTests
    {
        private StaticTicketRepository _repository;

        [SetUp]
        public void SetUp()
        {
            _repository = new StaticTicketRepository();
        }

        [Test]
        public void InsertTicket()
        {
            var ticketDescription = "ABC123";
            var ticketToAdd  = new Ticket(ticketDescription, null);

            // Act
            _repository.Insert(ticketToAdd);

            // Assert
            var actualTicket = _repository.GetById(ticketToAdd.Id);
            Assert.That(actualTicket, Is.EqualTo(ticketToAdd));
        }

        [Test]
        public void GetTicketById()
        {
            var ticketItem = new Ticket("ABC123", null);
            _repository.Insert(ticketItem);

            // Act
            var ticketResult = _repository.GetById(ticketItem.Id);

            //Assert
            Assert.That(ticketResult, Is.EqualTo(ticketItem));
        }

        [Test]
        public void MarkSolved()
        {
            var ticketItem = new Ticket("ABC123", null);
            _repository.Insert(ticketItem);

            // Act
            _repository.MarkSolved(ticketItem.Id);

            //Assert
            var ticketResult = _repository.GetById(ticketItem.Id);
            Assert.IsTrue(ticketResult.Solved);
        }
    }
}

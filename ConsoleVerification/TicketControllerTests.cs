﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Controllers;
using System.Web.Mvc;
using HelpDesk.Models;
using HelpDesk.Repositories;

namespace AppTests
{
    class TicketControllerTests
    {
        private StaticTicketRepository _repository;
        private TicketController _controller;

        [SetUp]
        public void SetUp()
        {
            _repository = new StaticTicketRepository();
            _controller = new TicketController(_repository);
        }

        [Test]
        public void ReturnIndexView()
        {
            var result = _controller.Index() as ViewResult;
            Assert.AreEqual("Index", result.ViewName);
        }

        [Test]
        public void ReturnAllView()
        {
            var result = _controller.All() as ViewResult;
            Assert.AreEqual("All", result.ViewName);
        }

        [Test]
        public void GenerateRandomTickets()
        {
            _controller.GenerateData(count: 10);

            var tickets = _repository.SelectAll();
            Assert.That(tickets.Count(),Is.EqualTo(10));
        }

        [Test]
        public void MarkTicketSolved()
        {
            var ticketItem = new Ticket("Test", null);
            _repository.Insert(ticketItem);

            // Act
            _controller.MarkSolved(ticketItem.Id);

            //Assert
            Assert.IsTrue(ticketItem.Solved);
        }

        [Test]
        public void AddTicket()
        {
            var ticketDescription = "ABC123";

            // Act
            _controller.Add(ticketDescription, null);
            var ticket = _repository.SelectAll().SingleOrDefault(x => x.Description == ticketDescription);

            // Assert
            Assert.IsTrue(ticket!=null);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Security.Policy;
using System.Web;

namespace HelpDesk.Models
{
    public class Ticket
    {
        public string Id { get; private set; }
        public string Description { get; private set; }
        public DateTime EntryDate { get; private set; }
        public DateTime? DueDate { get; private set; }
        public bool Solved { get; private set; }

        public Ticket(string description, DateTime? duedate = null)
        {
            Description = description;
            DueDate = duedate;
            EntryDate = DateTime.Now;
            Solved = false;
            Id = Guid.NewGuid().ToString();
        }

        public void MarkSolved()
        {
            this.Solved = true;
        }

    }

}
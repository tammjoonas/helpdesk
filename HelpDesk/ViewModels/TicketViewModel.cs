﻿using HelpDesk.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace HelpDesk.ViewModels
{
    public class TicketViewModel
    {
        public string Id { get; private set; }
        public string Description { get; private set; }
        public DateTime EntryDate { get; private set; }
        public DateTime? DueDate { get; private set; }
        public bool Solved { get; private set; }
        public string CssClassName { get; private set; }


        public TicketViewModel(Ticket ticket)
        {
            Id = ticket.Id;
            Description = ticket.Description;
            EntryDate = ticket.EntryDate;
            DueDate = ticket.DueDate;
            Solved = ticket.Solved;
            CssClassName = DueDate < DateTime.Now.AddHours(1) ? "danger" : null;
        }
    }

    public class PagedTicketViewModel
    {
        public IPagedList<TicketViewModel> TicketViewModelList { get; set; }
        public int TotalTicketCount { get; set; }
        public int FirstTicketNr { get; set; }
        public int LastTicketNr { get; set; }

        public PagedTicketViewModel(IOrderedEnumerable<Ticket> tickets, int pageSize, int page = 1)
        {
            List<TicketViewModel> ticketVMList = new List<TicketViewModel>();
            foreach (Ticket ticket in tickets)
            {
                var ticketViewModel = new TicketViewModel(ticket);
                ticketVMList.Add(ticketViewModel);
            }
            TicketViewModelList = ticketVMList.ToPagedList(page, pageSize);
            TotalTicketCount = tickets.Count();
            FirstTicketNr = TotalTicketCount > 0 ? page * pageSize - (pageSize - 1) : 0;
            LastTicketNr = TicketViewModelList.Count() > 0 ? FirstTicketNr + (TicketViewModelList.Count() - 1) : FirstTicketNr;
        }
    }

}
﻿using HelpDesk.Models;
using HelpDesk.ViewModels;
using PagedList;
using PagedList.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using HelpDesk.Repositories;

namespace HelpDesk.Controllers
{
    public class TicketController : Controller
    {
        public const int PAGE_SIZE = 10;
        //public static readonly List<Ticket> Tickets = new List<Ticket>();

        private ITicketRepository _ticketRepository;


        public TicketController(ITicketRepository repository)
        {
            this._ticketRepository = repository;
        }

        public ActionResult Index()
        {
            //Display only unsolved tickets on front page
            var ticketViewModels = new List<TicketViewModel>();
            var filteredTickets = _ticketRepository.SelectAll().Where(x => !x.Solved);
            var filteredSortedTickets = filteredTickets.OrderBy(x => !x.DueDate.HasValue)
                .ThenBy(x => x.DueDate);
            foreach (var ticket in filteredSortedTickets) 
            {
                var ticketViewModel = new TicketViewModel(ticket);
                ticketViewModels.Add(ticketViewModel);
            }            
            return View("Index", ticketViewModels);
        }

        public ActionResult All(int page = 1)
        {
            //Use pagination for "All" page
            var orderedTickets = _ticketRepository.SelectAll().OrderBy(x => x.DueDate);
            PagedTicketViewModel pagedTicketViewModel = new PagedTicketViewModel(orderedTickets, PAGE_SIZE, page);
            return View("All", pagedTicketViewModel);
        }

        [HttpPost]
        public ActionResult Add(string description, DateTime? duedate)
        {
            if (description != "")
            {
                var ticketItem = new Ticket(description, duedate);
                _ticketRepository.Insert(ticketItem);
            }
            return RedirectToAction("Index");

        }

        [HttpPost]
        public ActionResult MarkSolved(string id)
        {
            _ticketRepository.MarkSolved(id);
                
         
            return RedirectToAction("Index");

        }

        //RANDOM DATA GENERATION

        [HttpPost]
        public ActionResult GenerateData(int count)
        {
            var randomTicketGenerator = new RandomTicketGenerator();
            for (int i = 0; i < count; i++)
            {
                var ticketItem = randomTicketGenerator.CreateTicket();
                _ticketRepository.Insert(ticketItem);
            }
            return RedirectToAction("Index");

        }

    }

}
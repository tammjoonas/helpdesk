﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HelpDesk.Models;

namespace HelpDesk.Repositories
{
    public interface ITicketRepository
    {
        Ticket GetById(string id);
        IEnumerable<Ticket> SelectAll();
        void Insert(Ticket obj);
        void MarkSolved(string id);

    }
}

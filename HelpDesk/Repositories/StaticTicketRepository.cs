﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using HelpDesk.Models;

namespace HelpDesk.Repositories
{
    public class StaticTicketRepository : ITicketRepository
    {
        private static readonly List<Ticket> Tickets = new List<Ticket>();

        public Ticket GetById(string id)
        {
            return Tickets.SingleOrDefault(x => x.Id == id);
        }

        public void Insert(Ticket obj)
        {
            Tickets.Add(obj);
        }

        public void MarkSolved(string id)
        {
            var ticketItem = GetById(id);

            if (ticketItem != null)
            {
                ticketItem.MarkSolved();
            }
        }

        public IEnumerable<Ticket> SelectAll()
        {
            return Tickets.ToList();
        }


    }
}
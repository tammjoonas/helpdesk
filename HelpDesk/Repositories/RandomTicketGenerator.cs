using System;
using System.Text;
using HelpDesk.Models;

namespace HelpDesk.Repositories
{
    public class RandomTicketGenerator
    {

        public Ticket CreateTicket()
        {
            var description = RandomString(50);
            var duedate = RandomDate();
            var ticketItem = new Ticket(description, duedate);
            return ticketItem;
        }

        static readonly Random random = new Random();

        private string RandomString(int size)
        {
            var builder = new StringBuilder();
            char ch;
            for (int j = 0; j < size; j++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }

        private static DateTime RandomDate()
        {
            var fromDate = DateTime.Now.AddDays(-10);
            var range = DateTime.Now.AddDays(100) - fromDate;
            var randTimeSpan = new TimeSpan((long)(random.NextDouble() * range.Ticks));
            return fromDate + randTimeSpan;
        }
    }
}